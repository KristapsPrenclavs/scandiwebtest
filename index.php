
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="css/style.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>    
    <link rel="stylesheet" href="https://maxCDn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <title>Home Page</title>
</head>

<body>
<div class="header"><h1>Product list</h1></div>
<div class="link"><a href="addIndex.php">Add Items</a></div>
<hr>
<form method="POST">
    <div class="container">
        <div class="product-list">
            <?php 
            require_once $_SERVER['INCLUDE_PATH'].'app/Controller/list.php';
            require_once $_SERVER['INCLUDE_PATH'].'app/Controller/delete.php';         
            ?>  
        </div>       
    </div>
    <button class="del-btn" name="delete" type="submit">Delete Selected</button> 
</form> 
<hr>
</body>

<footer>    
    <div class="credentials"> Made By Kristaps Prenclavs. Contact me via - <a href="https:/gmail.com" target="_blank">kristaps.prenclavs@gmail.com</a></div>         
</footer>

</html> 