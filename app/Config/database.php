<?php

namespace app\Config;
include_once 'config.php';

class Database
{
    private $HOST = DB_HOST;
    private $DATABASE = DB_NAME;
    private $USERNAME = DB_USER;
    private $PASSWORD = DB_PASS;

    protected $db;
    
    protected function __construct() 
    {
        try 
        {            
            $pdo = new \PDO("mysql:host={$this->HOST};dbname={$this->DATABASE}", $this->USERNAME, $this->PASSWORD);
            
            $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
            
            $this->db = $pdo;
        } 
        catch (\PDOException $e) 
        {
            echo $e->getMessage();
        }
    }
}

  

