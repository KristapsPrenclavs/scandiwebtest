<?php

namespace app\Models;

use app\Config\Database;
use app\Interfaces\ProductInterface;
use app\Models\Product;

class ProductClass extends Database implements ProductInterface
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getAll(): array
    {
        $query = "SELECT * FROM items order by id ASC LIMIT 50";
        $stmt = $this->db->prepare($query);
        $stmt->execute();

        return $stmt->fetchAll(\PDO::FETCH_CLASS, Product::class);
    }

    public function create(Product $model): bool
    {
        $query = "INSERT INTO items (id, name, price, special) VALUES (:id, :name, :price, :special)";
        
        $stmt = $this->db->prepare($query);
    
        return $stmt->execute((array) $model);
    }

    public function delete($id): bool
    {
        $query = "DELETE FROM items WHERE id = :id";
        $stmt = $this->db->prepare($query);
        $stmt->bindParam(":id", $id);

        return $stmt->execute();
    }
}