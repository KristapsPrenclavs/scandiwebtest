<?php

namespace app\Interfaces;

require_once $_SERVER['INCLUDE_PATH'].'app/Models/products.php';

use app\Models\Product;

interface ProductInterface
{
   
    /**
     * Get all products
     * @return array
     */
    public function getAll(): array;

    /**
     * Create new product
     * @param $model for Product class
     * @return bool 
     */
    public function create(Product $model): bool;

    /**
     * Delete products
     * @param int $id
     * @return array
     */
    public function delete($id): bool;
}

