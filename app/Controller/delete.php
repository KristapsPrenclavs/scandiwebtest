<?php

require_once $_SERVER['INCLUDE_PATH'].'autoload.php';
require_once $_SERVER['INCLUDE_PATH'].'app/Models/products.php';

use app\Models\ProductClass;

try {              
        if (isset($_POST['delete']))
        {
            if(isset($_POST['check']))
            {
                foreach($_POST['check'] as $selected)
                {                        
                    $class = new ProductClass();
                    
                    $result = $class->delete($selected);  
                }
                echo '<script type="text/javascript">alert("Successfuly Deleted!")</script>';             
            }                                         
        }  
    } 
    catch (PDOException $err) 
    {
        echo $err->getMessage();
    }
