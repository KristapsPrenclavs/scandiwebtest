<?php

require_once $_SERVER['INCLUDE_PATH'].'autoload.php';
require_once $_SERVER['INCLUDE_PATH'].'app/Models/products.php';

use app\Models\ProductClass;
use app\Models\Product as ProductModel;

try {

    $model = new ProductModel;

    $_POST = filter_var_array($_POST, FILTER_SANITIZE_STRING);

    $id = $_POST['id'];
    
    $model->id = $_POST ['sku'].'_'.$id ?? '';
    $model->name = $_POST['name'] ?? '';
    $model->price = $_POST['price'] ?? '';
    $model->special = $_POST['special'] ?? '';
    
    $class = new ProductClass();
    $class->create($model);
    
    }
    catch (PDOException $err) 
    {
        echo $err->getMessage();
    }